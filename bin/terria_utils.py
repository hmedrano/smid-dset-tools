import sys
import requests
import json
import re
import argparse
from urllib.parse import quote
from pprint import pprint


def getERDDAPMetadataAttribute(endpoint,attribute,variableName='NC_GLOBAL'):
    ''' 
     Request a specific attribute in erddap metadata json file
    '''
    metadataRequest = requests.get(endpoint)
    if metadataRequest.status_code == 200:
        metadata = metadataRequest.json()        
        dmetadata=[]
        # Playing around and making a dictionary from the lists columnNames and array of rows
        for row in metadata['table']['rows']:
            drow = dict(zip(metadata['table']['columnNames'],row))
            dmetadata.append(drow)

        # make the search more organic, by column names
        for rowAttribute in dmetadata:            
            if rowAttribute['Variable Name'] == variableName and rowAttribute['Attribute Name'] == attribute:
                return rowAttribute['Value']

    return ''       

def getERDDAPMetadataGetVarList(endpoint):
    ''' 
     Request a specific attribute in erddap metadata json file
    '''
    metadataRequest = requests.get(endpoint)
    if metadataRequest.status_code == 200:
        metadata = metadataRequest.json()        
        dmetadata=[]
        # Playing around and making a dictionary from the lists columnNames and array of rows
        for row in metadata['table']['rows']:
            drow = dict(zip(metadata['table']['columnNames'],row))
            dmetadata.append(drow)

        vars=[]
        for rowAttribute in dmetadata:            
            if rowAttribute['Row Type'] == 'variable':
                vars.append(rowAttribute['Variable Name'])
        response={}
        for var in vars:
            response[var]={'attributes':[]}
            for rowAttribute in dmetadata:            
                if rowAttribute['Variable Name'] == var and rowAttribute['Row Type'] == 'attribute':
                    response[var]['attributes'].append({'name':rowAttribute['Attribute Name'], 
                                                        'type':rowAttribute['Data Type'],
                                                        'value':rowAttribute['Value'] })

        return response            
    return ''         



def getSortedVarListForCSV(varlist, firsts=['time'], exclude=[]):

    varnames = list(varlist.keys())
    # Exclude vars from wildcars?   
    if exclude:
        regexfilter=[]
        for exc in exclude:
          regexfilter.append( re.compile(exc) )

    sortedvarnames = []
    for fv in firsts:
        ocurrence=list(filter(lambda v: v.lower() == fv, varnames))[0]
        sortedvarnames.append( ocurrence )
        varnames.remove(ocurrence)
    sortedvarnames = sortedvarnames + varnames
    excludedvarnames=[]
    if exclude:
        for reg in regexfilter:
            excludedvarnames = excludedvarnames + list(filter(lambda i: reg.search(i), sortedvarnames))    
        excludedvarnames = list(set(excludedvarnames))
        sortedvarnames = [vname for vname in sortedvarnames if vname not in excludedvarnames]         

    longnames=[]
    units=[]
    for vn in sortedvarnames:
        ln=None ; u=None
        for att in varlist[vn]['attributes']:
            if att['name'] == 'long_name':
                ln = att['value']
            if att['name'] == 'units':
                u = att['value']
                if 'since' in u:
                    u = 'Date'

        longnames.append( ln if ln else vn)
        units.append( u if u else 'unit')

    return {'names' : sortedvarnames,
            'long_names' : longnames,
            'units' : units }
    


def parseTerriaEntrieToCSV(entrie):
    '''
        Parse the dictionary "entrie" and create the corresponding csv file that terria can read.
        The structure of the dictionary is:
         { header : [ <header names> , ... ]  
           rows-source : [ 
               { name : <str Name to id the row data> , 
                 data-endpoint : <data url endpoint of errdap in csvp output ending with ?> 
                 metadata-endpoint : <erddap url endpoint in json format> ,
                 columns : [
                     {
                         value : { from : metadata , attribute : <attribute name>, # Get the value from metadata-endpoint 
                                   encode : true                                   # Optional url encode the string
                                 }
                     },
                     {
                         value : { from : request ,                                # Get the value from a request to data-endpoint
                                   request : <Example "time,var1,var2&time>max(time)-1month"> 
                                 }
                     }
                     ...
                     ..
                     .
                 ] 
               }, 
               ...
               ..
               .
            ]
         }

    '''
       
    # Read header
    if 'header' in entrie:
        csvheader = ",".join(entrie['header']) + "\n"
    else:
        raise NameError('json without header')
        
    # Cycle in entrie['rows']
    if 'rows-source' in entrie:
        
        csvrows=""
        for current_row in entrie['rows-source']:
            dataEndpoint = current_row['data-endpoint'] if 'data-endpoint' in current_row else None
            metadataEndpoint = current_row['metadata-endpoint'] if 'metadata-endpoint' in current_row else None            
            # Start with the attribute-cols
            if ('columns' in current_row):
                csvcols=[]
                for col in current_row['columns']:

                    if isinstance(col['value'], str):
                        value = col['value']                        
                        if 'encode' in col:
                            if col['encode']:
                                value = quote(value,safe='')
                        csvcols.append(value)

                    elif isinstance(col['value'], dict):
                        
                        if col['value']['from'] == 'metadata':  # Get some attribute from metadata
                           
                            endpoint = col['value']['metadata-endpoint'] if 'metadata-endpoint' in col['value'] else metadataEndpoint
                            varWithAttribute = col['value']['variableName'] if 'variableName' in col['value'] else 'NC_GLOBAL' 
                            flagEncode = True if 'encode' in col['value'] else False
                            value = getERDDAPMetadataAttribute(endpoint,col['value']['attribute'], varWithAttribute)                            
                            if flagEncode:
                                value = quote(value,safe='')
                            csvcols.append(value) 

                        elif col['value']['from'] == 'request': # This is a data column, with csvp type

                            endpoint = col['value']['data-endpoint'] if 'data-endpoint' in col['value'] else dataEndpoint
                            if isinstance(col['value']['request'], list):
                                value=[]
                                for req in col['value']['request']:
                                    ival = endpoint + quote( req , safe='')
                                    value.append(ival)
                                csvcols.append( quote(','.join(value)) )
                            else:
                                request = quote(col['value']['request'], safe='')
                                value = endpoint + request
                                csvcols.append(value) 

                    else:
                        csvcols.append('None')

            csvrows = csvrows + ",".join(csvcols) + "\n"

    return csvheader + csvrows



# if __name__ == "__main__":

    # Parse arguments

    # Example python create_csv.py file.json

    # test a csv creation
    #fileparam = sys.argv[1]
    #entrie = json.load(open(fileparam,'r'))    
    #print(parseTerriaEntrieToCSV(entrie))
