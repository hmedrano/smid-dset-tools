#!/bin/sh 

DIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"

if [ -z "$1" ] ; then
   echo "Missing the ncml file argument"
   return 1
fi

if [ -z "$2" ] ; then 
   echo "Missing the ouput netcdf filename argument"	 
   return 1 
fi

if [ -z "$3" ] ; then
   XMEM="1g"
else 
   XMEM="$3"
fi   

java -Xmx${XMEM} -classpath $DIR/../lib/netcdfAll-4.6.11.jar ucar.nc2.dataset.NetcdfDataset -in $1 -out $2 -netcdf4 

