import numpy as np
import logging
import os, sys
import urllib.request, json
import requests
from geopy.distance import vincenty
from pygeoif import geometry
from czml import czml
from urllib.parse import quote
import erddap_utils


'''
   Los datos de entrada deben de venir de un conjunto de datos con convenciones 
   "climate and forecast" del tipo "Trajectory" (a series of data points along a 
   path through space with monotonically increasing times)

   Se solicitaran datos via un archivo csv o una ruta a un servicio erddap tipo 
   csvp (https://coastwatch.pfeg.noaa.gov/erddap/tabledap/documentation.html#fileType_csvp)

   1. mandar endpoint
   2. Validar si es cf compliant
   3. Devolver variables de coordenadas, para tiempo, latitud, longitud y profundidad (si existe)
        De no existir profundidad usar 0 en toda esa columna, para eso declarar una bandera.
   4. Definir si el muestreo es por minuto, hora, dia, o mes dependiendo del tamaño del dataset
        Calcular para [minuto, hora, dia...]
           Calcular el tamaño del dataset ( time&distinct()&orderByCount("") )  80 bytes por renglon.  
           Si es menor de <tamano-maximo> usar este muestreo.   (TODO buscar <tamano-maximo>)  Aprox. 4MB
   5. Obtener los datos en csvp para con el muestreo ideal.
       csvp debe venir tiempo, lat, lon, depth. 


'''

def getERDDAPMetadataAttribute(endpoint,attribute,variableName='NC_GLOBAL'):
    ''' 
     Request a specific attribute in erddap metadata json file
    '''
    metadataRequest = requests.get(endpoint)
    if metadataRequest.status_code == 200:
        metadata = metadataRequest.json()        
        dmetadata=[]
        # Playing around and making a dictionary from the lists columnNames and array of rows
        for row in metadata['table']['rows']:
            drow = dict(zip(metadata['table']['columnNames'],row))
            dmetadata.append(drow)

        # make the search more organic, by column names
        for rowAttribute in dmetadata:            
            if rowAttribute['Variable Name'] == variableName and rowAttribute['Attribute Name'] == attribute:
                return rowAttribute['Value']

    return ''       


def validateDataset(dataset_endpoint):
  '''
    Validar si el dataset es compliant con CF-1.6 FeatureType Trajectory.
    De no ser compliant retorna falso y reporta en warnings las razones.
    Al cumplir retorna un diccionario con las variables de coordenadas.

    Params
     : dataset_endpoint <ERDDAP_urls>
    Returns
     : Coordinate variables format <dict> keys=time, latitude, longitude, depth(optional)
  '''
  #TODO
  return { "time" : "time" , "latitude" : "latitude", "longitude" : "longitude", "depth" : "depth" }

def calculateIdealSample(dataset_endpoint, size_limit='10MB'):
  ''' 
   Calcular cual es el muestreo que esta dentro del limite de tamano. 
   los resultados pueden ser minute, hour, day, month ,... 
   De ser <None>, se utilizara la serie completa

    Params
     : dataset_endpoint <ERDDAP_urls>   
    Returns
     : Sample size inside "size_limit", can be [seconds,minute,day,month..]
  '''
  #TODO 
  return "day"

def parseCSVP(rawdata, coordvars):
  '''
    Recibe el archivo csvp en modo texto, se analiza y retorna
    una lista 2D con el tipo de dato apropiado, ademas de agregar
    la columna de profundidad si es que no existe.
  '''
  floatifposible = lambda v : float(v) if v.replace('.','',1).replace('-','',1).isdigit() else v if len(v)!=0 else 0
  if coordvars["depth"]:
    parseddata = [ [ floatifposible(col) for col in row.split(",")]  for row in rawdata.splitlines()[1:] ]
  else:
    # Agregar una columna para la profundidad, asumir cero para todas las muestras.
    parseddata = [ [ floatifposible(col) for col in row.split(",")] + [0]  for row in rawdata.splitlines()[1:] ]
  return (parseddata)  

def requestCSVP(dataset_endpoint, coordvars, samplesize):
  '''
    Solicitar los datos en csvp, y retornarlos en una lista con los datos  
     Returns
      : Two dimension list with row header ['time','longitude','latitude','depth'] 
        If depth not available then the depth column will have 0's 
  '''
  # Build url request  
  csvp_endpoint = dataset_endpoint.requesturl('csvp')

  request = (csvp_endpoint if csvp_endpoint.strip()[-1] == '?' else csvp_endpoint + "?") + \
            coordvars["time"] + "," + \
            coordvars["longitude"] + "," + \
            coordvars["latitude"] + \
            ("," + coordvars["depth"] if coordvars["depth"] else "") + \
            "&" + coordvars["time"] + "!=NaN" + \
            "&" + coordvars["longitude"] + "!=NaN" + \
            "&" + coordvars["latitude"] + "!=NaN"
  if samplesize:
    request += "&orderByClosest(\"time,1 " + samplesize + "\")"
  try:
    rawdata = requests.get(request).text
  except Exception as e:
    logging.error("Algo salio mal : " + str(e))
    sys.exit(1)

  return (parseCSVP(rawdata, coordvars))


def filterByPointDistance(data, kms=2):
  '''
    Regresar solo puntos que esten separados por una distancia de <kms>
  '''
  filtered=[]
  filtered.append(data[0])
  # location (lat, lon)
  lastlocation=(filtered[0][2], filtered[0][1])
  distance=0
  for row in data[1:]: 
    rlatlon = (row[2],row[1])
    try:
      distance += vincenty(lastlocation, rlatlon).km 
      if distance >= kms:
        filtered.append(row) 
        lastlocation = rlatlon
        distance = 0 
    except Exception as e:
      logging.error("Algo salio mal filtrando los puntos: " + str(e))      

  return filtered

def getCoverageRect(data):
  '''
   Returns <list> with the coverage rectangle [lonmin,latmin,lonmax,latmax]   
  '''
  lonmin,latmin,lonmax,latmax=None,None,None,None
  for row in data:
    rlatlon = (row[2],row[1])
    lonmin = rlatlon[1] if lonmin is None else rlatlon[1] if lonmin>rlatlon[1] else lonmin
    lonmax = rlatlon[1] if lonmax is None else rlatlon[1] if lonmax<rlatlon[1] else lonmax
    latmin = rlatlon[0] if latmin is None else rlatlon[0] if latmin>rlatlon[0] else latmin
    latmax = rlatlon[0] if latmax is None else rlatlon[0] if latmax<rlatlon[0] else latmax    
  return [lonmin,latmin,lonmax,latmax]

class CZML_wrapper:

  def __init__(self, **kwargs):
    '''
      Create header of czml, with clock information,
      Params:
       title, 
       interval : ISO 8601 str date <start_date>/<end_date>
    '''
    self.animation_multiplier="86400"
    self.interval=None    
    self.currentTime=None    
    self.title=""
    if "animation_multiplier" in kwargs:
      self.animation_multiplier = kwargs.get("animation_multiplier")    
    if "interval" in kwargs:
      self.interval = kwargs.get("interval")    
    if "currentTime" in kwargs:
      self.currentTime = kwargs.get("currentTime")
    if "title" in kwargs:
      self.title = kwargs.get("title")      

    self.doc = czml.CZML()
    # Intervalo de tiempo en el que existe el dataset
    ctime=czml.Clock(step="SYSTEM_CLOCK_MULTIPLIER", 
                    range="LOOP_STOP", 
                    multiplier=str(self.animation_multiplier),
                    interval=self.interval ,
                    currentTime=self.currentTime)

    # Encabezado del documento CZML siempre con id=document
    packet = czml.CZMLPacket(id="document",
                            name=self.title,
                            version="1.0", 
                            clock=ctime)                           
    self.doc.packets.append(packet)

  def addPoint(self, point_id,point_data, **kwargs):
    '''
     Params:
      point_data : Either tuple (time, lat, lon, depth)  or 2D List [(t,lat,lon,d),(t,lat,lon,d),...] 
    '''
    # Valores por defecto
    point_color = [0, 255, 255, 255]
    point_size = 10
    point_properties = {}
    point_title = self.title    
    if "point_color" in kwargs:
      point_color = kwargs.get("point_color")
    if "point_size" in kwargs:
      point_size  = kwargs.get("point_size")
    if "point_properties" in kwargs:
      point_properties  = kwargs.get("point_properties")    
    if "title" in kwargs:
      point_title = kwargs.get("title")        

    # Agregar la geometria de un PUNTO representando el movimiento del objeto
    # en el tiempo  
    unravel_data = [ col for row in point_data for col in row ] # desenredar la variable <data>
    point_position   = czml.Position(cartographicDegrees=unravel_data)
    point_color  = czml.Color(rgba=point_color)
    point_marker = czml.Point(pixelSize=point_size, show=True, color=point_color)  
    point_packet = czml.CZMLPacket(id=point_id+"_point",
                                  description=point_title,
                                  custom_properties=point_properties)  
    point_packet.position = point_position 
    point_packet.point    = point_marker
    point_packet.availability = time_coverage_start + "/" + time_coverage_end

    doc.packets.append(point_packet)      


def createCZML(data, dataset_endpoint, **kwargs):
  '''
    Crear formato CZML equivalente a partir de los datos [time,lat,lon,depth]
    en <data>, agregar encabezado al czml a partir de los metadatos ACDD disponibles
    en el url <metadata_endpoint>
     : kwargs
        point_color      : <rgba> Lista con 4 elementos - Color para el punto
        point_size       : <int> tamano del pixel
        point_properties : <dict> Propiedades personalizadas para agregar al punto.
        line_color       : <rgba> Lista con 4 elementos - Color para la linea
        line_width       : <int> grosor de la polylinea 
        line_properties  : <dict> Propiedades personalizadas para la polylinea.
  ''' 

  # Solicitar los metadatos requeridos, de metadata_endpoint
  metadata_endpoint = dataset_endpoint.metadataurl('json')
  time_coverage_start = getERDDAPMetadataAttribute(metadata_endpoint, "time_coverage_start")
  time_coverage_end = getERDDAPMetadataAttribute(metadata_endpoint, "time_coverage_end")
  title = getERDDAPMetadataAttribute(metadata_endpoint, "title")  

  # Valores por defecto
  point_color = [0, 255, 255, 255]
  point_size = 10
  point_properties = {}
  line_color = [0, 155, 155, 155]
  line_width = 2
  line_properties = {}
  animation_multiplier="86400"
  if "point_color" in kwargs:
    point_color = kwargs.get("point_color")
  if "point_size" in kwargs:
    point_size  = kwargs.get("point_size")
  if "point_properties" in kwargs:
    point_properties  = kwargs.get("point_properties")
  if "line_color" in kwargs:
    line_color  = kwargs.get("line_color")    
  if "line_width" in kwargs:
    line_width  = kwargs.get("line_width")        
  if "line_properties" in kwargs:
    line_properties  = kwargs.get("line_properties") 
  if "animation_multiplier" in kwargs:
    animation_multiplier = kwargs.get("animation_multiplier")    

  pretty_json = False
  if "pretty_json" in kwargs:
    pretty_json = kwargs.get("pretty_json")

  #
  doc = czml.CZML()
  # Intervalo de tiempo en el que existe el dataset
  ctime=czml.Clock(step="SYSTEM_CLOCK_MULTIPLIER", 
                   range="LOOP_STOP", 
                   multiplier=str(animation_multiplier),
	                 interval=time_coverage_start + "/" + time_coverage_end ,
                   currentTime=data[0][0])

  # Encabezado del documento CZML siempre con id=document
  packet = czml.CZMLPacket(id="document",
                           name=title,
                           version="1.0", 
                           clock=ctime)                           
  doc.packets.append(packet)

  # Agregar la geometria de un PUNTO representando el movimiento del objeto
  # en el tiempo  
  unravel_data = [ col for row in data for col in row ] # desenredar la variable <data>
  point_data   = czml.Position(cartographicDegrees=unravel_data)
  point_color  = czml.Color(rgba=point_color)
  point_marker = czml.Point(pixelSize=point_size, show=True, color=point_color)  
  point_packet = czml.CZMLPacket(id=dataset_endpoint.datasetid+"_point",
                                 description=title,
                                 custom_properties=point_properties)  
  point_packet.position = point_data 
  point_packet.point    = point_marker
  point_packet.availability = time_coverage_start + "/" + time_coverage_end

  doc.packets.append(point_packet)

  # Agregar la geometria de una POLYLINE que representa toda la trayectoria 
  # recorrida 
  unravel_data_latlondepth = [ row[1:] for row in data ]
  # TODO unravel_data_latlondepth debe de ser en tuplas o listas (lon,lat,dept),(..)..
  line = geometry.LineString(unravel_data_latlondepth)
  polyline_data = czml.Positions(cartographicDegrees=line)
  polyline_color = czml.Color(rgba=line_color)
  polyline_material = czml.Material()
  polyline_material.solidColor = { 'color' : polyline_color}
  polyline = czml.Polyline(show=True, 
                           width=line_width, 
                           followSurface=False, 
                           positions=polyline_data, 
                           material=polyline_material)

  polyline_packet = czml.CZMLPacket(id=dataset_endpoint.datasetid+"_polyline",
                                    custom_properties=line_properties)
  polyline_packet.polyline = polyline

  doc.packets.append(polyline_packet)

  if pretty_json:
    return (doc.dumps(indent=2, sort_keys=True))
  else:
    return (doc.dumps())



if __name__ == "__main__":
    
  # Testing
  datasetendpoint = erddap_utils.ERDDAP_urls("http://dataserver.cigom.org/erddap", 'GMOG_SG622_0002_RAW', 'tabledap')

  cv = validateDataset(datasetendpoint)
  data = requestCSVP(datasetendpoint, cv, calculateIdealSample(datasetendpoint))
  filtered = filterByPointDistance(data)
  #print(len(data))
  #print(len(filtered))
  print(createCZML(filtered, datasetendpoint,                  
                    point_properties={ "name" : "SG622-M0002-RAW" , 
                                      "datasource" : "http://dataserver.cigom.org/erddap/tabledap/GMOG_SG622_0002_RAW.html"},
                    pretty_json=True))