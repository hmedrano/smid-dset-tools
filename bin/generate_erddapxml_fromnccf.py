#!/usr/bin/env python

import os,sys
import re
import logging 
import random, string
import netCDF4 as nc
import numpy as np
import json
import argparse
from compliance_checker.cf import CFBaseCheck
from compliance_checker import cfutil


def utilget_dataType(dsvariable):
    if dsvariable.dtype == np.dtype('int8'):
        return 'byte'
    elif dsvariable.dtype == np.dtype('int16'):
        return 'short'
    elif dsvariable.dtype == np.dtype('int32'):
        return 'int'        
    elif dsvariable.dtype == np.dtype('int64'):
        return 'long' 
    elif dsvariable.dtype == np.dtype('float32'):
        return 'float' 
    elif dsvariable.dtype == np.dtype('float64'):
        return 'double'
    elif dsvariable.dtype == np.dtype('bool'):
        return 'boolean'
    elif dsvariable.dtype == np.dtype('S1'):
        return 'String'
    else:
        return 'String'

def has_atts_required(ds, featureType, report=False):
    """
      Validar si el dataset ds contienen los atributos globales necesario para 
      funcionar en ERDDAP, en general el dataset debe de ser CF compliant y contener los 
      atributos que ERDDAP requiere para las Discrete Sampling Geometries.
      https://coastwatch.pfeg.noaa.gov/erddap/download/setupDatasetsXml.html#cdm_data_type
      https://coastwatch.pfeg.noaa.gov/erddap/download/setupDatasetsXml.html#globalAttributes

       :ds netCDF4 Dataset
       :featureType (timeseries,timeseries-profile-single-station, etc)
       :report
    """

    atts_infile_required_fields = [
      'title' , 'summary' , 'featureType', 'cdm_data_type' 
    ]
    if featureType == 'timeseries':
        atts_infile_required_fields += ['cdm_timeseries_variables']
    elif featureType == 'timeseries-profile-single-station':
        atts_infile_required_fields += ['cdm_timeseries_variables', 'cdm_profile_variables']            

    atts_required_fields_defaults = {
       'cdm_data_type' : featureType,
       'naming_authority' : 'cigom.org' , 
       'institution': 'CIGOM' ,
       'project' : 'Consorcio de Investigación del Golfo de México',
       'Conventions' : 'CF-1.6, ACDD-1.3',
       'infoUrl' : 'https://www.cigom.org', 
       'sourceUrl' : '(local files)',
       'license' : 'licencia publica CIGOM'
    }    

    return _has_atts_required(ds, atts_infile_required_fields, atts_required_fields_defaults, report)


def _has_atts_required(ds, atts_infile_required_fields, atts_required_fields_defaults, report=False):
    """     
     :param netCDF4.Dataset ds : Netcdf or Dap dataset    
    """
    atts_valid = True
    atts_required_fields = [ key for key in atts_required_fields_defaults.keys() ]

    for field in atts_infile_required_fields:
      try:
        fT = ds.getncattr(field)
        logging.debug('Atributo encontrado : ' + field + ' = ' + str(fT))
        atts_valid &= True
      except:
        logging.error('No se encontro el atributo ' + field + '. Es mandatorio que exista dentro del netcdf.')
        atts_valid &= False

    needed_global_atts = {}
    for field in atts_required_fields:
      try:
        fT = ds.getncattr(field)
        logging.debug('Atributo encontrado : ' + field + ' = ' + str(fT))        
      except:
        logging.warning('No se encontro el atributo ' + field + '. Es mandatorio que exista. Se agregara al xml.')        
        needed_global_atts[field] = atts_required_fields_defaults[field]

    if report:
        return atts_valid , needed_global_atts
    else:
        return atts_valid


def erddap_nccf_compliance_check(ds, featureType, report=False):
    """
        En esta funcion revisamos si existen en el dataset los atributos globales requeridos.
        Revisamos si existen las variables de coordenadas para cumplir con la Climate&Forecast, 
        (time,lat,lon,z)
        Ademas de revisar que existan las variables que contienen los atributos cf_role 
        (timeseries_id, profile_id, trajectory_id) 
    """

    erddap_valid = False
    erddap_valid, xml_global_atts = has_atts_required(ds, featureType, report=True)
    coord_report = {}
    if not erddap_valid:
        logging.error('Es muy probable que el <dataset> no funcione correctamente en ERDDAP hasta que se agreguen los atributos necesarios.')
        erddap_valid = False
    else:
        erddap_valid = True

    time_variable = cfutil.get_time_variable(ds)
    if not time_variable:
        logging.error('No encontramos ninguna variable que identifique la coordenada de tiempo. No es posible continuar.')       
        erddap_valid = False
    else:
        coord_report['time'] = time_variable

    lat_variable = ds.get_variables_by_attributes(axis='Y',standard_name='latitude')
    if not lat_variable:
        logging.error('No encontramos ninguna variable que identifique la coordenada de latitud. No es posible continuar.')       
        erddap_valid = False
    else:
        coord_report['latitude'] = lat_variable[0].name

    lon_variable = ds.get_variables_by_attributes(axis='X',standard_name='longitude')
    if not lon_variable:
        logging.error('No encontramos ninguna variable que identifique la coordenada de longitud. No es posible continuar.')           
        erddap_valid = False
    else:
        coord_report['longitude'] = lon_variable[0].name        

    # timeseries
    if featureType == 'timeseries':
        z_variable = ds.get_variables_by_attributes(axis='Z')
        if not z_variable:
            logging.error('No encontramos ninguna variable que identifique la coordenada Z. No es posible continuar.')           
            erddap_valid = False
        else:
            coord_report['z'] = z_variable[0].name
            
        var_timeseries_id = ds.get_variables_by_attributes(cf_role='timeseries_id')
        if not var_timeseries_id:
            logging.error('Ninguna variable define el atributo cf_role="timeseries_id", es mandatorio para ERDDAP, no es posible continuar.')        
            erddap_valid = False
        else:
            coord_report['timeseries_id'] = var_timeseries_id[0].name     

    # timeseries-profile-single-station
    elif featureType == 'timeseries-profile-single-station':
        z_variable = ds.get_variables_by_attributes(axis='Z')
        if not z_variable:
            logging.error('No encontramos ninguna variable que identifique la coordenada Z. No es posible continuar.')           
            erddap_valid = False
        else:
            coord_report['z'] = z_variable[0].name
            if not coord_report['z'].lower() in ['depth','altitude']:
                xml_global_atts['cdm_altitude_proxy'] = coord_report['z']
        
        var_profile_id = ds.get_variables_by_attributes(cf_role='profile_id')
        if not var_profile_id:
            logging.error('Ninguna variable define el atributo cf_role="profile_id", es mandatorio para ERDDAP, no es posible continuar.')
            erddap_valid = False
        else:
            coord_report['profile_id'] = var_profile_id[0].name

        var_timeseries_id = ds.get_variables_by_attributes(cf_role='timeseries_id')
        if not var_timeseries_id:
            logging.error('Ninguna variable define el atributo cf_role="timeseries_id", es mandatorio para ERDDAP, no es posible continuar.')        
            erddap_valid = False
        else:
            coord_report['timeseries_id'] = var_timeseries_id[0].name            

    if report:
        return erddap_valid, xml_global_atts, coord_report
    else:
        print ("" + ds.filepath() + " ERDDAP compliant: " + str(erddap_valid))
        print (coord_report)

    
#def build_nccf_erddap_xml(ds, featureType, cf_report, fileDir, fileNameRegex, excludeVarRegex=[], global_attributes=None):
def build_nccf_erddap_xml(ds, featureType, filedir, filename, excludeVarRegex=[], global_attributes=None):
    """
        Funcion que construye las etiquetas xml <dataset> para ERDDAP
         ds: (netCDF4.Dataset) 
         featureType: (string) any of ['timeseries', 'timeseries-profile-single-station']
         filedir: (string) Path to dataset
         filename: (string) Name of netcdf/ncml/dap dataset
         excludeVarRegex: (list) List of strings containing a regular expression, that excludes variables that match.
         global_attributes: (dict)  Dictionary of additional global attributes to add to the xml config file.
    """

    output=[]
    valid_dataset = False    
    valid_dataset, xml_global_attributes ,coord_report = erddap_nccf_compliance_check(ds, featureType, True)

    if global_attributes:
        xml_global_attributes = { **xml_global_attributes, **global_attributes}

    random_id = ''.join([random.choice(string.ascii_lowercase + string.digits) for l in range(10)])
    dataset_id = filename.translate( str.maketrans('.','_'))[0:10] + "_" + random_id

    output.append('<dataset type="EDDTableFromNcCFFiles" datasetID="' + dataset_id + '" active="true">')
    # erddap config
    output.append(' <fileDir>' + filedir + '</fileDir>')
    output.append(' <fileNameRegex>' + filename + '</fileNameRegex>')
    output.append(' <reloadEveryNMinutes>10080</reloadEveryNMinutes>')
    output.append(' <updateEveryNMillis>10000</updateEveryNMillis>')
    output.append(' <fileTableInMemory>false</fileTableInMemory>')
    output.append(' <accessibleViaFiles>false</accessibleViaFiles>')     

    # dataset globals
    output.append(' <addAttributes>')
    # append existing global attributes in a xml comment block
    output.append('  <!-- sourceAttributes>')
    for gatt in ds.ncattrs():
        output.append('   <att name="' + gatt + '">' + ds.getncattr(gatt) + '</att>')    
    output.append('  </sourceAttributes -->')
    # append xml_global_attributes
    for gatt in xml_global_attributes:
        output.append('  <att name="' + gatt + '">' + xml_global_attributes[gatt] + '</att>')    
    output.append(' </addAttributes>')

    # dataset variables
    vars = set(ds.variables.keys()) 

    # Exclude vars from wildcars?   
    if excludeVarRegex:
        regexfilter=[]
        for exc in excludeVarRegex:
          regexfilter.append( re.compile(exc) )

    excludedvars=[]
    # exclude coordinate variables    
    needed_coordvars = ['time', 'latitude', 'longitude']    
    if featureType == 'timeseries':
        if not coord_report['timeseries_id'] in needed_coordvars:
            needed_coordvars.append('timeseries_id')
        if not coord_report['z'] in needed_coordvars:
            needed_coordvars.append('z')            

    elif featureType == 'timeseries-profile-single-station':
        if not coord_report['timeseries_id'] in needed_coordvars:
            needed_coordvars.append('timeseries_id')
        if not coord_report['profile_id'] in needed_coordvars:
            needed_coordvars.append('profile_id') 
        if not coord_report['z'] in needed_coordvars:
            needed_coordvars.append('z')                        

    for coordvar in needed_coordvars:
        if coordvar in coord_report.keys():
            if coord_report[coordvar] != None:
                excludedvars.append(coord_report[coordvar])
    #                 

    if excludeVarRegex:    
        for reg in regexfilter:
            excludedvars = excludedvars + list(filter(lambda i: reg.search(i), vars))    
    excludedvars = list(set(excludedvars))
    usable_vars = [vname for vname in list(vars) if vname not in excludedvars]
    usable_vars.sort() 
    
    # coordinate variables
    output.append('  <!-- coordinate variables -->')
    for coordvar in needed_coordvars:
        output.append('  <dataVariable>')
        output.append('   <sourceName>' + coord_report[coordvar] + '</sourceName>')
        if coordvar == 'z' or coordvar in ['timeseries_id','profile_id','trajectory_id']:
            output.append('   <destinationName>' + coord_report[coordvar] + '</destinationName>')    
        else:
            output.append('   <destinationName>' + coordvar + '</destinationName>')
        output.append('   <dataType>' + utilget_dataType(ds.variables[coord_report[coordvar]]) + '</dataType>') 

        # append in dataset attributes inside a xml comment
        output.append('   <!-- sourceAttributes>')
        for vatt in ds.variables[coord_report[coordvar]].ncattrs():
            output.append('     <att name="' + vatt + '">' + str(ds.variables[coord_report[coordvar]].getncattr(vatt)) + '</att>')
        output.append('   </addAttributes -->')
        # add custom variable attributes accordingly
        #for vatt in xml_variable_attributes:
        #    if vatt['variable_name'] == cf_report[coordvar]['variable_name']:
        #        output.append('   <addAttributes>')
        #        for att in vatt['atts'].keys():
        #            output.append('     <att name="' + att + '">' + vatt['atts'][att] + '</att>')
        #        output.append('   </addAttributes>')
        output.append('  </dataVariable>')
    output.append('  <!-- coordinate variables end -->')

    for varname in usable_vars:
        output.append('  <dataVariable>')
        output.append('   <sourceName>' + varname + '</sourceName>')
        output.append('   <destinationName>' + varname + '</destinationName>')
        output.append('   <dataType>' + utilget_dataType(ds.variables[varname]) + '</dataType>')
        # append in-dataset attribytes inside a xml comment                
        output.append('   <!-- sourceAttributes>')
        for vatt in ds.variables[varname].ncattrs():
            output.append('     <att name="' + vatt + '">' + str(ds.variables[varname].getncattr(vatt)) + '</att>')
        output.append('   </sourceAttributes -->')
        #TODO Variable add attributes
        
        
        output.append('  </dataVariable>')

    output.append('</dataset>')

    return "\n".join(output)    



if __name__ == "__main__":

    # generate_erddapxml_fromnccf.py  
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset_cf_compliant",  type=str, help="Ruta al dataset Climate & Forecat compliant (netcdf,opendap).")
    parser.add_argument("feature_type", choices=["timeseries","timeseries-profile-single-station"], help="Feature type segun la convención CF del dataset.")
    parser.add_argument("filedir", type=str, action="store", help="Ruta al dataset, que va a la etiqueta <fileDir>.")
    parser.add_argument("filename", type=str, action="store", help="Nombre del dataset, que va a la etiqueta <fileNameRegex>.")
    parser.add_argument("-r","--erddap-ready", action="store_true", help="Construir el xml de salida, listo para reemplazar datasets.xml de erddap.")
    parser.add_argument("-a","--attributes", type=str, action="store", help="Atributos globales adicionales, en notación json.")
    parser.add_argument("-e","--exclude-regex", action="append", type=str, help="Expresión regular para excluir variables del xml.")
    parser.add_argument("-o","--output", action="store", type=str, help="Escribir la salida a este archivo.")

    args = parser.parse_args()  
    #print (parse_metatable(args.SMID_form_url, auth_key=args.api_key,build=args.output,indent=args.indent))

    try:
        ds = nc.Dataset(args.dataset_cf_compliant,'r')
    except Exception as e:
        logging.error("Algo salio mal al intentar leer " + args.dataset_cf_compliant)  
        logging.error("Exception: " + str(e))
        sys.exit(1)

    parsed_attributes = None  
    if args.attributes:
        parsed_attributes = json.loads(args.attributes)         

    xmlstr = build_nccf_erddap_xml(ds, args.feature_type, args.filedir, args.filename, global_attributes=parsed_attributes) 

    if args.erddap_ready:
        xmlstr = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n<erddapDatasets>\n\n" + \
                 xmlstr + \
                 "\n\n</erddapDatasets>\n"      

    if args.output:
        fb = open(args.output,'w')
        fb.write(xmlstr)
        fb.close()
    else:
        print(xmlstr)


    



#ds = nc.Dataset('../../sample-data/bomm2_its_level2_30min_SAMPLE_timeseriesprofile.nc','r')
#out = build_nccf_erddap_xml(ds,'timeseries-profile-single-station', '/datafiles/', \
#                            'bomm2_its_level2_30min_SAMPLE_timeseriesprofile.nc' , \
#                            global_attributes={'subsetVariables':'station,time,latitude,longitude'})
#print (out)