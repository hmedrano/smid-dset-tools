import os
import requests
from urllib.parse import quote

class ERDDAP_urls:
  def __init__(self, erddapurl, datasetid, protocol):
    ''' 
      The erddap url , the dataset id, The protocol can be 'tabledap' or 'griddap'
    '''
    self.erddapurl = erddapurl 
    self.datasetid = datasetid
    self.protocol = protocol

  def metadataurl(self,dataformat='json'):
    return os.path.join(self.erddapurl, "info", self.datasetid , "index." + dataformat )  

  def requesturl(self,dataformat='csvp', **kwargs):
    varlist=None
    if "varlist" in kwargs:
      varlist = kwargs.get("varlist")
    options=None
    if "options" in kwargs:
      options = kwargs.get("options")

    response = os.path.join(self.erddapurl, self.protocol, self.datasetid + "." + dataformat )
    if varlist:
      if type(varlist) is list:
        vparams = ','.join(varlist)
      else:
        vparams = varlist 
      response += '?' + quote(vparams,safe='')
    if options:
      if type(options) is list:
        oparams = '&'.join(options)
      else:
        oparams = options 
      response += '&' + quote(oparams,safe='')      
    return response

  def getAttribute(self,attribute,variableName='NC_GLOBAL'):
      ''' 
      Request a specific attribute in erddap metadata json file
      '''
      metadataRequest = requests.get(self.metadataurl())
      if metadataRequest.status_code == 200:
          metadata = metadataRequest.json()        
          dmetadata=[]
          # Playing around and making a dictionary from the lists columnNames and array of rows
          for row in metadata['table']['rows']:
              drow = dict(zip(metadata['table']['columnNames'],row))
              dmetadata.append(drow)

          # make the search more organic, by column names
          for rowAttribute in dmetadata:            
              if rowAttribute['Variable Name'] == variableName and rowAttribute['Attribute Name'] == attribute:
                  return rowAttribute['Value']

      return ''       

  def getVarList(self, varlist=None):
    ''' 
     
    '''
    metadataRequest = requests.get(self.metadataurl())
    if metadataRequest.status_code == 200:
        metadata = metadataRequest.json()        
        dmetadata=[]
        # Playing around and making a dictionary from the lists columnNames and array of rows
        for row in metadata['table']['rows']:
            drow = dict(zip(metadata['table']['columnNames'],row))
            dmetadata.append(drow)

        vars=[]
        for rowAttribute in dmetadata:            
            if rowAttribute['Row Type'] == 'variable':
                vars.append(rowAttribute['Variable Name'])
        response={}

        for var in vars:
            if type(varlist) is list:
              addvar = True if var in varlist else False
            else:
              addvar=True
            if addvar:
              response[var]={'attributes':[]}
              for rowAttribute in dmetadata:            
                  if rowAttribute['Variable Name'] == var and rowAttribute['Row Type'] == 'attribute':
                      response[var]['attributes'].append({'name':rowAttribute['Attribute Name'], 
                                                          'type':rowAttribute['Data Type'],
                                                          'value':rowAttribute['Value'] })              

        return response            
    return ''        