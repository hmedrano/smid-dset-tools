#!/usr/bin/env python

import sys
import os
import re
from urllib.request import Request, urlopen
from jinja2 import Environment, FileSystemLoader
import argparse
import netCDF4 as nc 
import logging
import json 


META_TABLE = { "id" :                   { "value_id" : "<filename>" , "type" : "string", "default_value" : "" } ,
               "naming_authority" :     { "default_value" : "cigom.org" , "type" : "string" } ,
               "Metadata_Conventions" : { "default_value" : "Unidata Dataset Discovery v1.3" , "type" : "string"} ,
               "Conventions" :          { "default_value" : "CF-1.6, ACDD-1.3" , "type" : "string"} ,
               "Metadata_Link" :        { "value_id" : "urldelgrupo", "default_value" : "smid.cigom.org" , "type" : "string"} ,
               # title section
               "title" :                { "value_id" : "titulo_dataset", "type" : "string"} ,
               "summary" :              { "value_id" : "resumen_proposito", "type" : "string"} ,
               "keywords" :             { "value_id" : "palabras_clave" , "type" : "string" } ,
               "keywords_vocabulary" :  { "value_id" : "vocab" , "type" : "string" , "default_value" : "" } ,
               "standard_name_vocabulary" : { "value_id" : "vocab" , "type" : "string" , "default_value" : "CF-1.6" } , 
               "history" :              { "type" : "string" , "default_value" : None } ,
               "comment" :              { "value_id" : "comentarios" , "type" : "string" , "default_value" : "" } ,
               "geospatial_lat_min" :   { "value_id" : "ubicacion" , "field" : 1 , "type" : "string" , "default_value" : None } ,
               "geospatial_lat_max" :   { "value_id" : "ubicacion" , "field" : 0 , "type" : "string" , "default_value" : None } ,
               "geospatial_lon_min" :   { "value_id" : "ubicacion" , "field" : 4 , "type" : "string" , "default_value" : None } ,
               "geospatial_lon_max" :   { "value_id" : "ubicacion" , "field" : 3 , "type" : "string" , "default_value" : None } ,
               "geospatial_vertical_min" :   { "value_id" : "ubicacion" , "field" : 8 , "type" : "string" , "default_value" : None } ,
               "geospatial_vertical_max" :   { "value_id" : "ubicacion" , "field" : 7 , "type" : "string" , "default_value" : None } ,
               "time_coverage_start" :   { "value_id" : "rangotemp" , "field" : 0 , "type" : "string" , "default_value" : None } ,
               "time_coverage_end" :   { "value_id" : "rangotemp" , "field" : 1 , "type" : "string" , "default_value" : None } ,
               # extent info
               "geospatial_lat_units" :  { "value_id" : "ubicacion" , "field" : 2 , "type" : "string" , "default_value" : None } ,
               "geospatial_lat_resolution" :   { "value_id" : "<calculte>" , "type" : "float" , "default_value" : None } ,
               "geospatial_lon_units" :  { "value_id" : "ubicacion" , "field" : 5 , "type" : "string" , "default_value" : None } ,
               "geospatial_lon_resolution" :   { "value_id" : "<calculte>" , "type" : "float" , "default_value" : None } ,
               "geospatial_vertical_units" :   { "value_id" : "resoluciondedatos" , "field" : 2 ,"type" : "string" , "default_value" : None, "multi" : True } , 
               "geospatial_vertical_resolution" :   { "value_id" : "" , "type" : "string" , "default_value" : None } , #TODO
               "geospatial_vertical_positive" :   { "value_id" : "ubicacion" , "field" : 6 , "type" : "string" , "default_value" : None } ,
               # time coverage
               "time_coverage_duration" : { "value_id" : "rangotemp" , "field" : 2 , "type" : "string" , "default_value" : None } ,
               "time_coverage_resolution" :   { "value_id" : "rangotemp" , "field" : 3 , "type" : "string" , "default_value" : None } ,
               # Creator search
               "acknowledgment" :       { "value_id" : ["recher","recloc"], "type" : "string", "default_value" : "Investigación apoyada por el CONACYT y la Secretaría de Energía, a través del Fondo Sectorial CONACYT-Secretaría de Energía-Hidrocarburos, proyecto 201441. Contribución del Consorcio de Investigación del Golfo de México (CIGoM)", "append_value" : "Investigación apoyada por el CONACYT y la Secretaría de Energía, a través del Fondo Sectorial CONACYT-Secretaría de Energía-Hidrocarburos, proyecto 201441. Contribución del Consorcio de Investigación del Golfo de México (CIGoM)" } ,
               "creator_email" :        { "value_id" : "contactodelgrupo" , "type" : "string", "default_value" : None} ,
               "creator_name" :         { "value_id" : "nombredelgrupo" , "type" : "string", "default_value" : None} ,
               "creator_url" :          { "value_id" : "urldelgrupo" , "type" : "string", "default_value" : None} ,
               "date_created" :         { "value_id" : "" , "type" : "string", "default_value" : None} ,
               "date_modified" :        { "value_id" : "<date>" , "type" : "string", "default_value" : None} ,
               "date_issued" :          { "value_id" : "<date>" , "type" : "string", "default_value" : None} ,
               "institution" :          { "value_id" : "org" , "type" : "string", "default_value" : "CIGOM"} ,
               "project" :              { "type" : "string", "default_value" : "Consorcio de Investigación del Golfo de México"} ,
               "contributor_name" :     { "value_id" : "colaboradores" , "field" : [0,3] , "type" : "string", "default_value" : None, "multi" : True} ,
               "contributor_role" :     { "value_id" : "colaboradores" , "field" : 1 , "type" : "string", "default_value" : None, "multi" : True} ,
               "publisher_name" :       { "type" : "string", "default_value" : "Sistema de Manejo Integral de Datos CIGOM"} ,
               "publisher_url" :        { "type" : "string", "default_value" : "http://smid.cigom.org"} ,
               "publisher_email" :      { "type" : "string", "default_value" : "smid@cigom.org"} ,
               "license" :              { "value_id" : "licdatos",  "type" : "string", "default_value" : "Licencia CIGOM"},
               "processing_level" :     { "value_id" : "procdatos",  "type" : "string", "default_value" : None}
               }


def buildattfromvalueid(pform, id, field=None, separator='; ', value_idx=0):
  """
   Hace una busqueda de la etiqueta 'id' en el formulario ckan 'var', 
   ademas de ser necesario, con un campo especifico. 
   Esta funcion es wrapper de la original _form_getvaluefrominputid, aqui revisamos si recibimos,
   en los argumentos una lista o un elemento.
   Regresa el valor de la etiqueta, o etiquetas, separadas por ';'. De no existir regresa None.
  """
  if type(id) is list:
    if type(field) is list:
      return separator.join([ _form_getvaluefrominputid(pform, id[idx],field[idx],value_idx) for idx,val in enumerate(id) ])      
    else:
      return separator.join([ _form_getvaluefrominputid(pform, id[idx],field,value_idx) for idx,val in enumerate(id) ])      
  else:
    return _form_getvaluefrominputid(pform,id,field,value_idx)  


def _form_getvaluefrominputid(pform, id, field=None, value_idx=0):
  _input = _form_getinputbyid(pform,id)
  if _input:
      if field is None:
        return _input["value"].strip()
      else:
        return _input["value"][value_idx][field].strip()    
  else:
    return None    


def _form_getinputbyid(pform, id):
  """
   Busca en el formulario pform, por un input con campo 'id'
   igual al del argumento.
   Regresa el elemento 'input' del formulario.
  """
  for sname, _section in pform['sections'].items():    
    for iname, _input in _section["inputs"].items():
      if _input["id"] == id: 
        return _input 
  return None

def _form_elementsinvalueid(pform,id):
  """
   Regresa el numero de elementos en el campo 'value', de 
   (ser una lista) un 'input' en pform, ubicado por argumento id.
  """
  _input = _form_getinputbyid(pform, id)
  if _input:    
    if type(_input["value"]) is list:
      return len(_input["value"])
    else:
      return 1
  return None

# 

def loadformjson(jsonpath, auth_key):
  """
   Con esta funcion cargamos y parseamos el json del formulario de ckan.
   El argumento jsonpath puede ser la ruta remota, o local. 
   Regresa el formulario analizado en un diccionario, o None en caso de fallo.
  """
  try:
    if 'http' in jsonpath:
      request = Request(jsonpath)
      # request.add_header('Authorization','0887a0e7-b5ce-4a0a-af79-2db1e082a79e')
      request.add_header('Authorization', auth_key)
      raw_response = urlopen(request).read()    
      response = re.sub(r'<meta(.*)<\/script>','', raw_response.decode('utf-8').replace('\n',''))     
    else:
      fb = open(jsonpath,'r')
      response = json.load(fb)
      fb.close()    
  except Exception as e:
    logging.error("Something went wrong : " + str(e))
    return None

  return response

def meta2ncml(dmetadata, indent=2):
  """
   Construye usando el sistema de plantillas jinja el formato ncml a partir de los 
   atributos globales listados en dmetadata. Retorna el template construido.
  """
  file_loader = FileSystemLoader( os.path.join( os.path.dirname(os.path.abspath(__file__)), '../lib/templates/') )  
  env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
  # template = env.get_template('ncml-template.jnj')
  template = env.get_template('ncml-template-clean.jnj')
  dmetadata['indent_spaces'] = indent
  return (template.render(metadata=dmetadata))

def parse_metatable(formjsonfilepath, auth_key=None, build="ncml", indent=2):
  """
   Recibe una ruta al json de un formulario de smid, 
    Ej. http://smid.cigom.org/form/api/json/439dac16-b36c-4b2e-bc52-f93c8f7d3063  o una ruta local.
   Y recupera de este formulario los campos especificados en la tabla META_TABLE, para
   despues imprimir el resultado en la plantilla especificada, puede ser ncml, json o comandos nco   
  """

  parsedform = json.loads( loadformjson(formjsonfilepath, auth_key) )  
  global_atts = {}
  for name,atts in META_TABLE.items():    
    
    if "value_id" in atts:
      if atts["value_id"] == "<filename>": 
        formvalue = os.path.basename(formjsonfilepath).replace(".json","")
      else:
        if "multi" in atts and atts["multi"] == True:          
          formvalue = []
          for vidx in range(_form_elementsinvalueid(parsedform, atts["value_id"])):
            field = atts["field"] if "field" in atts else None
            if type(field) is list:
                formvalue.append( buildattfromvalueid(parsedform, [atts["value_id"]] * len(field), field, separator=', ', value_idx=vidx) )
            else:
              formvalue.append( buildattfromvalueid(parsedform, atts["value_id"], field, separator=' , ', value_idx=vidx) )
          formvalue = ' ; '.join(formvalue)          
        else:
          field = atts["field"] if "field" in atts else None
          formvalue = buildattfromvalueid(parsedform, atts["value_id"], field)

        if "append_value" in atts:
          formvalue += " ; " + atts["append_value"]
    else:
      formvalue = None

    if formvalue:
      global_atts[name] = formvalue
    elif "default_value" in atts and atts["default_value"]:
      global_atts[name] = atts["default_value"]
    else:
      logging.debug("Not using attribute : " + str(name))

  if build == 'ncml':
    return (meta2ncml(global_atts, indent))
  elif build == 'json':
    return (json.dumps(global_atts, indent=2,sort_keys=True))



if __name__ == "__main__":

  # parsesmidform.py  
  parser = argparse.ArgumentParser()
  parser.add_argument("SMID_form_url",  type=str, help="Ruta al archivo json del formulario de metadatos smid 'FORMATO DE ENTREGA DE DATOS V4.1.1'")
  parser.add_argument("-k","--api-key", action="store", help="Llave para acceder al api de CKAN.",default="")
  parser.add_argument("-o","--output", type=str, choices=["ncml", "nco"], help="Forma en la que se entregan los metadatos.", default="ncml")
  parser.add_argument("-i","--indent", type=int, help="Numero de caracteres a identar la salida", default=2)

  args = parser.parse_args()  
  print (parse_metatable(args.SMID_form_url, auth_key=args.api_key,build=args.output,indent=args.indent))
  

#TODO Agregar plantillas con biblioteca jinja para salida tipo comandos NCO



