#!/bin/sh

XMEM="1G"
if [ ! -z "$1" ] ; then 
   XMEM="$1"
fi 

docker run -e ERDDAP_XMX_SIZE="$XMEM" -e ERDDAP_XMS_SIZE="$XMEM" -w /usr/local/tomcat/webapps/erddap/WEB-INF -v $PWD/:/datafiles/ --rm -it hmedrano/smid-erddap:1.82 bash GenerateDatasetsXml.sh

