#!/bin/sh

if [ -z "$1" ] ; then
   echo "Missing the local datasets.xml file argument."
   return 1
fi
LOCAL_DATASETXML="$(realpath $1)"

XMEM="2G"
if [ ! -z "$2" ] ; then 
   XMEM="$2"
fi 

PORT="8080"
if [ ! -z "$3" ] ; then
   PORT="$3"
fi


docker run --rm -i -p $PORT:8080 -e ERDDAP_XMX_SIZE="$XMEM" -e ERDDAP_XMS_SIZE="$XMEM" -v $PWD/:/datafiles/ -v $LOCAL_DATASETXML:/usr/local/tomcat/content/erddap/datasets.xml hmedrano/smid-erddap:1.82
