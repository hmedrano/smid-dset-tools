#!/bin/bash

DIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"
[[ ":$PATH:" != *":${DIR}/bin:"* ]] && export PATH=$DIR/bin:$PATH
[[ ":$PYTHONPATH:" != *":${PYTHONPATH}/bin:"* ]] && export PYTHONPATH=$DIR/bin:$PYTHONPATH

echo "Finished adding $DIR/bin/ directory to the PATH, and PYTHONPATH"

