# smid-dset-tools

Herramientas para la preparación de datasets para su ingreso a la plataforma smid.

Requerimientos

python 3.6
jinja2
netCDF4 
geopy
pygeoif
git+https://github.com/hmedrano/czml


Lista herramientas 
smid-tools
 
 ERDDAP - tabledap
  
  evarlist
  - Listar a partir de un endpoint, datos de variables
    Params: 
     endpoint <str>
     lista-variables: <list>
     lista-variables-excluir: <str regex>

