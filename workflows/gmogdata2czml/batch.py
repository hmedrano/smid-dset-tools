import trayectory2czml as t2czml
import erddap_utils 
import terria_utils

'''
 http://dataserver.cigom.org/erddap/tabledap/GMOG_SG623_0003_RAW.html
 http://dataserver.cigom.org/erddap/tabledap/GMOG_SG622_0002_RAW.html 
 http://dataserver.cigom.org/erddap/tabledap/GMOG_SG624_0001_RAW.html
'''


gmogm = [ { 'id' : 'GMOG_SG623_0003_RAW' , 'color' : [0, 255, 255, 255] , 'line-color' : [0, 155, 155, 155],
            'tsvars' : ['time','depth','temperature','salinity','speed','sigma_t', 'dissolved_oxygen_sat','aanderaa4330_dissolved_oxygen','density']} ,
          { 'id' : 'GMOG_SG622_0002_RAW' , 'color' : [255, 0, 255, 255] , 'line-color' : [155, 0, 155, 155],
            'tsvars' : ['time','depth','temperature','salinity','speed','sigma_t', 'dissolved_oxygen_sat','aanderaa4330_dissolved_oxygen','density']} ,
          { 'id' : 'GMOG_SG624_0001_RAW' , 'color' : [255, 255, 0, 255] , 'line-color' : [155, 155, 0, 155],
            'tsvars' : ['time','depth','temperature','salinity','speed','sigma_t', 'dissolved_oxygen_sat','aanderaa4330_dissolved_oxygen','density']} ] 

for idx,tdesc in enumerate(gmogm):

  # The erddap object
  mission = erddap_utils.ERDDAP_urls('http://dataserver.cigom.org/erddap', tdesc['id'], 'tabledap')
  # Calculate the best sample size to keep the czml lees than the maximum size (10mb)
  sampletimesize = t2czml.calculateIdealSample(mission)
  # Get the variables attributes of the variables listed in tdesc['tsvars']
  varatts = mission.getVarList(tdesc['tsvars'])
  # Sort the varatts results so that time and depth come first in order, 
  # This returns only variable name, long names and units
  var4terria = terria_utils.getSortedVarListForCSV(varatts, firsts=['time','depth'])
  # Build the url request to get csvp of the variables tdescp['tsvars'] and options
  csvpurl = mission.requesturl(varlist=var4terria['names'], options='orderByClosest("time,1 ' + str(sampletimesize) + '")')
  # Validate the erddap object is a trayecctory type, returns the coordinate variabls
  cv = t2czml.validateDataset(mission)
  # Request the data
  data = t2czml.requestCSVP(mission, cv, sampletimesize)
  # Filter the data, so get only the points that are separated by more than 2kms
  filtered = t2czml.filterByPointDistance(data, kms=2)
  # Get the coverage rectangle of the trayectory data
  rect = t2czml.getCoverageRect(data)

  properties4featuretype = { "name" : mission.datasetid,   
                             "title" : mission.getAttribute('title'), 
                             "summary" : mission.getAttribute('summary'),
                             "temperature-profile-plot" : "/proxy/_1d/http://dataserver.cigom.org/erddap/tabledap/" + mission.datasetid + ".png?time,depth,temperature&temperature!=NaN&.draw=markers&.marker=6%7C3&.color=0x000000&.colorBar=%7C%7C%7C%7C%7C&.bgColor=0xffccccff&.yRange=0%7C1050%7Cfalse",
                             "temperature-profile-plot-big" : "/proxy/_1d/http://dataserver.cigom.org/erddap/tabledap/" + mission.datasetid + ".largePng?time,depth,temperature&temperature!=NaN&.draw=markers&.marker=6%7C3&.color=0x000000&.colorBar=%7C%7C%7C%7C%7C&.bgColor=0xffccccff&.yRange=0%7C1050%7Cfalse",                             
                             "salinity-profile-plot" : "/proxy/_1d/http://dataserver.cigom.org/erddap/tabledap/" + mission.datasetid + ".png?time,depth,salinity&salinity!=NaN&.draw=markers&.marker=6%7C3&.color=0x000000&.colorBar=%7C%7C%7C%7C%7C&.bgColor=0xffccccff&.yRange=0%7C1050%7Cfalse",
                             "salinity-profile-plot-big" : "/proxy/_1d/http://dataserver.cigom.org/erddap/tabledap/" + mission.datasetid + ".largePng?time,depth,salinity&salinity!=NaN&.draw=markers&.marker=6%7C3&.color=0x000000&.colorBar=%7C%7C%7C%7C%7C&.bgColor=0xffccccff&.yRange=0%7C1050%7Cfalse",
                             "timecoverage-start" : data[0][0],
                             "timecoverage-end" : data[-1][0],
                             "data-source-url" : csvpurl,
                             "data-source-name" : "Series de Tiempo",
                             "preview-var-col" : "2" , 
                             "preview-var-name" : "temperature (degree C)",
                             "source-names" : ','.join(var4terria['names']), 
                             "long-names" : ','.join(var4terria['long_names']),
                             "units" : ','.join(var4terria['units']) }      

  # Method that creates the czml file
  czml = t2czml.createCZML(filtered, mission,
          point_color=tdesc['color'],
          line_color=tdesc['line-color'],
          animation_multiplier="172800",
          point_properties=properties4featuretype,
          line_properties=properties4featuretype,
          pretty_json=True)

  fb = open(mission.datasetid + ".czml","w")
  fb.write(czml)
  fb.close()

  print ("czml file: " + mission.datasetid + ".czml")
  print ("czml rect: " + str(rect))
  print ("ts variable names: " + ','.join(var4terria['names']))
  print ("ts variable long names: " + ','.join(var4terria['long_names']))
  print ("ts variable units: " + ','.join(var4terria['units']))
  print ("ts endpoint: " + csvpurl)
  print ("")
  


