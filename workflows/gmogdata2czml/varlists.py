import terria_utils
import erddap_utils
import trayectory2czml as t2czml

eurl = erddap_utils.ERDDAP_urls("http://dataserver.cigom.org/erddap", 'GMOG_SG623_0003_RAW', 'tabledap')

varlist = ['time','depth','temperature','salinity','dissolved_oxygen_sat','density']

varatts = eurl.getVarList(varlist)
print( eurl.requesturl(varlist=varlist, options='orderByClosest("time,1 day")'))

# print ( varlist.keys())
var4terria = terria_utils.getSortedVarListForCSV(varatts, firsts=['time','depth'])
print ( ','.join(var4terria['names']) )
print ( ','.join(var4terria['long_names']) )
print ( ','.join(var4terria['units']) )