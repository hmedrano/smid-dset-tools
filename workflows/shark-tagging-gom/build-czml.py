import pandas as pd
import trayectory2czml as t2czml
import erddap_utils 
import terria_utils


# Parse csv with pandas
tagg_df = pd.read_csv('tagging-data-gm-gom-new.csv')

# unique animals
animal_ids = tagg_df['animal_id'].unique() 
animal_dfs = []

rows_for_csvp = ['dateisostr','lat','lon','depth_new','days_vect','time_mins_since_00','dist2tagging','dist2former_loc','time2former_loc','trav_rate_kmh']

for aid in animal_ids:
    adf = tagg_df[tagg_df['animal_id']==aid]
    adf['dateiso'] =  pd.to_datetime(adf['new_date_vector'], format='%d/%m/%Y %H:%M')
    adf['dateisostr'] =  adf['dateiso'].dt.strftime("%Y-%m-%d %H:%M:%SZ")    
    print ("Animal id : " + str(aid))
    print (len(adf.index))

    # convert dataframe to list
    print (adf[rows_for_csvp].values.tolist())
    animal_dfs.append(adf[rows_for_csvp])

rows_for_csvp = ['dateisostr','lat','lon','depth_new','days_vect','time_mins_since_00','dist2tagging','dist2former_loc','time2former_loc','trav_rate_kmh']


# print(df.head())