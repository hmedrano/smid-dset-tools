#!/bin/sh

API_KEY=$(cat smid_api_key)
#FORM_URL="http://smid.cigom.org/form/api/json/439dac16-b36c-4b2e-bc52-f93c8f7d3063"

# PERSEO GOM Daily SST
FORM_URL="http://smid.cigom.org/form/api/json/f25eb28e-a684-4fc9-9673-60140ec3cfa5"

# PERSEO GOM Monthly Climatology SST
#FORM_URL="http://smid.cigom.org/form/api/json/05fa128d-e936-4806-8a14-e803987b500a"

# PERSEO GOM Monthly Climatology SST Anomalies
#FORM_URL="http://smid.cigom.org/form/api/json/fce187bb-509e-4b18-8e57-e2d8c7b045df"

echo "parse_datasubform.py $FORM_URL -k API_KEY -o ncml"

parse_datasubform.py $FORM_URL -k $API_KEY -o ncml
